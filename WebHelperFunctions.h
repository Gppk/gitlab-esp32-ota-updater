#ifndef WEB_HELPER_FUNCTIONS_H
#define WEB_HELPER_FUNCTIONS_H

#include <Arduino.h>

String getReleaseId(String recentReleaseQueryResponse);

String getMostRecentReleaseFromGitlab(const String projectID, String &releaseName );

String sendGetMessage(String url);

#endif