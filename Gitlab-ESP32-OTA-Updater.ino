#include <WiFi.h>
#include "WebHelperFunctions.h"
#include "FirmwareUpdateHelpers.h"

// Your WiFi credentials. Ideally stored in an arduino_secrets.h
// /You will need to create that file and put the following two variables in it!
// const char* ssid = "";
// const char* password = "";
// File with SSID & Passwords not stored in git
#include "arduino_secrets.h"


// Get this in your project settings on github
const String projectID = "";


void setup() {
  Serial.begin(115200);
  
  beginWifi();

  String releaseName = "";
  String mostRecentReleaseUrl = getMostRecentReleaseFromGitlab(projectID);
  getFirmwareFileAndUpdate(mostRecentReleaseUrl);
}

// No loop functionality required as the device will be reflashed before it runs it hopefully!
void loop() {}

// Start up and connect to an access point.
void beginWifi() {
  // Start WiFi connection
  WiFi.mode(WIFI_MODE_STA);        
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");  
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}
