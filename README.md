# GitLab Pipeline Auto Build and ESP32 Reflash Library

[![License](https://img.shields.io/badge/License-MIT-blue.svg)](LICENSE)

This is a simple library that enables automatic building and release of GitLab pipelines, followed by automatic download and reflashing of firmware on an ESP32 development board. It simplifies the process of continuous integration and deployment for ESP32 projects, allowing you to automate the firmware update workflow.

## Features

- Streamlines the GitLab pipeline workflow for ESP32 projects
- Automates the build and release process
- Downloads the latest firmware release from GitLab
- Reflashes the firmware onto an ESP32 development board
- Simplifies continuous integration and deployment for ESP32 projects

## Installation

To use this library, follow these steps:

1. Clone this repository to your local machine or download the ZIP file.

```shell
git clone https://gitlab.com/Gppk/gitlab-esp32-ota-updater.git
```

2. Install the required dependencies using your preferred package manager.

    2.1 You wil lneed to ensure you have the Arduino and ESP32 libraries installed. I'm not going to cover that but [here](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/) is some guidance.

    2.2 Install [ArduinoJSON](https://arduinojson.org/)



3. Configure the library by editing the following places:

    3.1 `setup-build.sh` - Ensure you add the correct arduino libraries for your specific Arduino code at the bottom

    3.2 `version` - Create a version number for your release. 

    3.3 `arduino_secrets.h` - Create an arduino_secrets.h fie and add the following code:

        const char* ssid = "your_ssid";
        const char* password = "ssid_pw";

    3.4 `Gitlab-ESP32-OTA-Updater.ino` - Set the `projectID` variable to the project ID of your gitlab repo.

    3.5 `.gitlab-ci.yml` - Change all of the < tags > to your specific naming.


4. Check the structure of all your code matches that shown in `.gitlab-ci.yml` and upload the file to your main gitlab pipeline file. 

## Usage

To use the library, follow these steps:

1. Ensure that your ESP32 development board is connected to your computer.

2. Use the library as shown in the main .ino file in this repo, ensure your actual gitlab repo is building some other ESP code that you want to flash onto your device.

3. The library will automatically trigger the GitLab pipeline for your project, build the firmware, and create a release.

4. Once the release is created, you can then use the EPS32 code in the library will download the latest firmware release from GitLab.

5. The library will then flash the firmware onto your ESP32 development board.

6. You will receive notifications and progress updates throughout the process in the serial monitor window.

## Contributing

Contributions are welcome! If you encounter any issues or have suggestions for improvements, please open an issue or submit a pull request on the [GitHub repository](https://github.com/your-username/gitlab-pipeline-esp32-reflash).

## License

This project is licensed under the [MIT License](LICENSE). Feel free to modify and distribute this code as needed.