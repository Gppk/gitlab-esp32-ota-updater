#ifndef FIRMWARE_UPDATE_HELPERS_H
#define FIRMWARE_UPDATE_HELPERS_H

#include <Arduino.h>

// If this function returns at all, it didn't work.
// If this function succeeds your ESP will update
void getFirmwareFileAndUpdate(String firmwareFileHttpUrl);

// Function to update firmware incrementally
// firmware is written to the device until server closes
void AddFirmwareUpdateIncrement(uint8_t *data, size_t len);

#endif